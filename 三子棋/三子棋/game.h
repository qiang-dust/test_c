#pragma once
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define ROW 3
#define COL 3
void Initboard(char board[ROW][COL],int row,int col);
//初始化
void Displayboard(char board[ROW][COL], int row, int col);
//打印棋盘
void Playmove(char board[ROW][COL], int row, int col);
//玩家下棋
void Computermove(char board[ROW][COL], int row, int col);
//1.*玩家赢
//2.#电脑赢
//3.Q平局
//4.C继续
char Iswin(char board[ROW][COL], int row, int col);