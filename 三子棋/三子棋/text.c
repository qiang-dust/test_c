#define _CRT_SECURE_NO_WARNINGS

#include"game.h"

void menu()
{
	printf("******************************\n");
	printf("****** 1.play    0.exit  *****\n");
	printf("******************************\n");
}

void game()
{
	char ret ='C';
	char board[ROW][COL] = { 0 };
	//初始化
	Initboard(board, ROW, COL);
	//打印棋盘
	Displayboard(board, ROW, COL);

	while (1)
	{
		//玩家下棋
		Playmove(board, ROW, COL);
		Displayboard(board, ROW, COL);
		ret = Iswin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		//电脑下棋
		Computermove(board, ROW, COL);
		Displayboard(board, ROW, COL);
		ret = Iswin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		//判断输赢
	}
	if (ret == '*')
		printf("恭喜玩家获胜\n");
	else if (ret == '#')
		printf("电脑赢了，你个小菜鸟\n");
	else
		printf("平局，连电脑都比不过\n");
}

int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择：");
		scanf("%d", &input);
		switch (input)
		{
		case 0:
			printf("退出游戏");
			break;
		case 1:
			game();
			break;
		default:
			printf("输入错误，请再输一遍");
		}
	} while (input);
		return 0;
}