#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>

//void Print(int x)
//{
//	if (x <= 9)
//	{
//		printf("%d\n", x);
//		return x;
//	}
//	else
//		Print(x / 10);
//	printf("%d\n", x % 10);
//}
//
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//    Print(a);
//	return 0;
//}

//int Fac(int x)
//{
//	if (x == 1)
//		return 1;
//	else
//		return x * Fac(x - 1);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fac(n);
//	printf("%d", ret);
//	return 0;
//}

//int main()
//{
//	int i,n = 0,sum=1;
//	scanf("%d", &n);
//	for (i = 1; i <= n; i++)
//	{
//		sum *= i;
//	}
//	printf("%d", sum);
//	return 0;
//}

//int my_strlen(char* x)
//{
//	int n = 0;
//	if (*x != '\0')
//		return 1 + my_strlen(x+1);
//	else
//		return 0;
//	
//}
//
//int main()
//{
//	char a[] = "abcdrf";
//	int n = my_strlen(a);
//	printf("%d", n);
//	return 0;
//}
//int my_strlen(char* x)
//{
//	int n = 0;
//	while (*x != '\0')
//	{
//		*(x++);
//		n++;
//	}
//	return n;
//}
//
//int main()
//{
//	char a[] = "abcdef";
//	int n = 0;
//	n=my_strlen(a);
//	printf("%d", n);
//	return 0;
//}


//实现各个位数变为个位数相加
//int Digsum(int x)
//{
//	if (x <= 9)
//		return x;
//	else
//		return Digsum(x / 10) + x % 10;
//}
//
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int ret = Digsum(a);
//	printf("%d", ret);
//	return 0;
//}

//实现N的K次方
//int square(int x, int y)
//{
//	if (y > 1)
//		return square(x,y-1) * x;
//	else
//		return 1;
//
//}
//
//int main()
//{
//	int n = 2, k = 0;
//	scanf("%d", &k);
//	int ret = square(n,k);
//	printf("%d", ret);
//	return 0;
//}

//int Fab(int x)
//{
//	if (x <= 2)
//		return 1;
//	else
//		return Fab(x - 2) + Fab(x - 1);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fab(n);
//	printf("%d", ret);
//	return 0;
//}

//非递归实现斐波那列数
//int main()
//{
//	int i, a = 1, b = 1, c = 1, n = 0;
//	scanf("%d", &n);
//	switch (n)
//	{
//	case 1:
//		c = 1;
//		break;
//	case 2:
//		c = 1;
//		break;
//	default:
//		for (i = 3; i <= n; i++)
//		{
//			c = a + b;
//			a = b;
//			b = c;
//		}
//		break;
//	}
//	printf("%d", c);
//	return 0;
//
//}

//迭代的方式实现字符串的逆序
//int main()
//{
//	char arr[] = "abcdef";
//	int left = 0, right = strlen(arr)-1;
//	while (left < right)
//	{
//		char temp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = temp;
//		left++;
//		right--;
//	}
//	printf("%s", arr);
//	return 0;
//}

//递归方式实现字符串的逆序

void reverse_string(char* string)
{
	char temp = *string;//1.先把第一个字母保存
	int len= strlen(string)-1;
	*(string) = *(string + len);//2.把F给A
	*(string + len) = '\0';//3.f的位置附上\0
	if (strlen(string+1) >= 2)
		reverse_string(string + 1);
	*(string + len) = temp;
}

int main()
{
	char arr []= "abcdef";
	reverse_string(arr);
	printf("%s", arr);
	return 0;
}