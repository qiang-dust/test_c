#include <stdio.h>
#define S(x) x*x
#define T(x) S(x)/S(x)+1

int main()
{
    int k = 3, j = 2;
    printf("%d,%d", S(k + j), T(k + j));
}
